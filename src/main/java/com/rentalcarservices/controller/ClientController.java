package com.rentalcarservices.controller;

import com.rentalcarservices.model.Client;
import com.rentalcarservices.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/add")
    public String clientAdd(Model model){
        model.addAttribute("newClient", new Client());
        return "client_new_form";
    }

    @PostMapping("/add")
    public String clientAdd(Client client) {

        clientService.add(client);
        return "redirect:/client/list";
    }
    @GetMapping("/list")
    public String clientList(Model model){
        model.addAttribute("listOfClient", clientService.getClientList());
        return "client_list";
    }

    @GetMapping("/delete/{clientIdentifier}")
    public String deleteClient(@PathVariable Long clientIdentifier){
        clientService.deleteClientById(clientIdentifier);
        return "redirect:/client/list";
    }


}
