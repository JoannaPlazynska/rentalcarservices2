package com.rentalcarservices.service;

import com.rentalcarservices.model.Car;
import com.rentalcarservices.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public void add(Car car){
        carRepository.save(car);
    }

    public List<Car> getCarList(){
        return  carRepository.findAll();
    }

    public void deleteCarById(Long carIdentifier){
        carRepository.deleteById(carIdentifier);
    }

    public Optional<Car> findCar(Long id){
        return carRepository.findById(id);
    }

}
