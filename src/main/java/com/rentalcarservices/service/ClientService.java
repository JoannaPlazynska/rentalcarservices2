package com.rentalcarservices.service;

import com.rentalcarservices.model.Car;
import com.rentalcarservices.model.Client;
import com.rentalcarservices.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public void add(Client client){
        clientRepository.save(client);
    }

    public List<Client> getClientList(){
        return  clientRepository.findAll();
    }

    public void deleteClientById(Long clientIdentifier){
        clientRepository.deleteById(clientIdentifier);
    }

    public Optional<Client> findClient(Long id){
        return clientRepository.findById(id);
    }
}
