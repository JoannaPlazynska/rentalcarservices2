package com.rentalcarservices.service;

import com.rentalcarservices.model.Employee;
import com.rentalcarservices.model.RentCar;
import com.rentalcarservices.repository.RentCarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class RentCarService {

    @Autowired
    private RentCarRepository rentCarRepository;

    public void add(RentCar rentCar){
        rentCarRepository.save(rentCar);
    }

    public List<RentCar> getRentCarList(){
        return rentCarRepository.findAll();
    }

    public void deleteRentCarById(Long rentCarIdentifier){
        rentCarRepository.deleteById(rentCarIdentifier);
    }

    public Optional<RentCar> findRentCar(Long id){
        return rentCarRepository.findById(id);
    }
}
