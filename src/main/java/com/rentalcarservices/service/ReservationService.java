package com.rentalcarservices.service;

import com.rentalcarservices.model.RentCar;
import com.rentalcarservices.model.Reservation;
import com.rentalcarservices.repository.RentCarRepository;
import com.rentalcarservices.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    public void add(Reservation reservation){
        reservationRepository.save(reservation);
    }

    public List<Reservation> getReservationList(){
        return reservationRepository.findAll();
    }

    public void deleteReservationById(Long reservationIdentifier){
        reservationRepository.deleteById(reservationIdentifier);
    }

    public Optional<Reservation> findReservation(Long id){
        return reservationRepository.findById(id);
    }
}
